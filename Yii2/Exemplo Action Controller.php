    public function actionReportApontamento() {
        $request = Yii::$app->request->post();
        
        $nomeRelatorio = "apontamento";
        $model_relatorio = [];

        array_push($model_relatorio, [
            'campo' => 'endereco_imagem',
            'tipo' => 'string',
            'valor' => "endereco da imagem",
        ]);
        array_push($model_relatorio, [
            'campo' => 'string_sql',
            'tipo' => 'string',
            'valor' => "WHERE do SQL",
        ]);

        $parametros = Report::montaParametroXML($model_relatorio);
        $curl_post_data = Report::montaParametroCurlPost($nomeRelatorio, 'es', 'bd_escola_sabatina', $parametros, "PDF");
        $retorno = Report::chamaRelatorio('endereco onde vai gravar o arquivo retorno/arquivo/', "@es", $nomeRelatorio, $curl_post_data);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $retorno;
    }
