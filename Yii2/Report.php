<?php

namespace common\components;

class Report {

    const URL_SERVICE_JASPER = 'http://104.131.39.180:8080/HgwsJasper/gerarRelatorio';
    const URL_BANCO_POSTGRES = 'jdbc:postgresql://localhost:5432/';
    const JASPER_PASTA_TEMPORARIA = '/srv/jasper/es/pdfs';
    const JASPER_PASTA_JASPER = '/srv/jasper/es';

    public static function montaParametroXML($model_relatorio) {
        $parametros = '';
        if (is_array($model_relatorio) && count($model_relatorio) > 0) {
            foreach ($model_relatorio as $value) {
                $parametros .= <<<EOT
<entry>
    <string>{$value['campo']}</string>
    <{$value['tipo']}>{$value['valor']}</{$value['tipo']}>
</entry>
EOT;
            }
        }
        return $parametros;
    }

    public static function montaParametroCurlPost($nomeRelatorio, $aplicacao, $banco, $parametros, $formato) {
        $urlBanco = self::URL_BANCO_POSTGRES . $banco;
        $pastaTmp = self::JASPER_PASTA_TEMPORARIA;
        $pastaJasper = self::JASPER_PASTA_JASPER;

        $curl_post_data = <<<EOT
<relatorio>
  <nome>{$nomeRelatorio}</nome>
  <parametros>
    {$parametros}
  </parametros>
  <formato>{$formato}</formato>
  <aplicacao>{$aplicacao}</aplicacao>
  <nomeRelatorioGerado>{$nomeRelatorio}</nomeRelatorioGerado>
  <url>{$urlBanco}</url>
  <pasta_tmp>{$pastaTmp}</pasta_tmp>
  <pasta_jasper>{$pastaJasper}</pasta_jasper>
</relatorio>
EOT;
        return $curl_post_data;
    }

    public static function chamaRelatorio($url, $sistema, $nomeRelatorio, $curl_post_data) {
        $curl = curl_init(self::URL_SERVICE_JASPER);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);

        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }

        if (strlen($curl_response) > 600) {
            $directory = \Yii::getAlias($sistema . '/web/arquivo/');

            if (isset(\Yii::$app->user->identity->usuario)) {
                $filename = 'temp_' . date('YmdHis') . '_' . \Yii::$app->user->identity->usuario . '_' . $nomeRelatorio . '.pdf';
            } else {
                $filename = 'temp_' . date('YmdHis') . '_' . $nomeRelatorio . '.pdf';
            }

            $fp = fopen($directory . $filename, "w+");
            fputs($fp, $curl_response);
            fclose($fp);
            return ['status' => 'success', 'mensagem' => '', 'url' => $url . $filename];
        } else {
            return ['status' => 'error', 'mensagem' => $curl_response, 'url' => null];
        }
    }

}
