package br.com.acstoigo.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Chrystian Toigo
 */
public enum TipoFormatoArquivo {

    PDF("PDF"),
    CSV("CSV"),
    HTML("HTML"),
    XLSX("XLSX"),
    ODS("ODS"),
    XLS("XLS");
    private final String codigo;
    private static final Map<String, TipoFormatoArquivo> lookup = new HashMap<String, TipoFormatoArquivo>();
    // ------------------------------

    static {
        for (TipoFormatoArquivo s : TipoFormatoArquivo.values()) {
            lookup.put(s.getCodigo(), s);
        }
    }
    // ------------------------------	

    private TipoFormatoArquivo(String codigo) {
        this.codigo = codigo;
    }

    String codigo() {
        return this.codigo;
    }

    // ------------------------------
    /**
     * @return
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo
     * @return
     */
    public static TipoFormatoArquivo get(String codigo) {
        return lookup.get(codigo.toUpperCase());
    }

    @Override
    public String toString() {
        return codigo;
    }
}
