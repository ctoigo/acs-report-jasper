package br.com.acstoigo.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.util.HashMap;

/**
 * @author Chrystian Toigo
 */
@XStreamAlias("relatorio")
public class Relatorio {

    private String nome;
    private HashMap<String, Object> parametros;
    private String formato;
    private String aplicacao;
    private String nomeRelatorioGerado;
    
    private String url;
    private final String usuario = "postgres";
    private final String senha = "SENHA";
    private String pasta_tmp;
    private String pasta_jasper;
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public HashMap<String, Object> getParametros() {
        return parametros;
    }

    public void setParametros(HashMap<String, Object> parametros) {
        this.parametros = parametros;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public void setNomeRelatorioGerado(String nomeRelatorioGerado) {
        this.nomeRelatorioGerado = nomeRelatorioGerado;
    }

    public String getNomeRelatorioGerado() {
        return nomeRelatorioGerado;
    }

    public void setAplicacao(String aplicacao) {
        this.aplicacao = aplicacao;
    }

    public String getAplicacao() {
        return aplicacao;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getUrl() {
        return url;
    }

    public void setPastaTmp(String pasta_tmp) {
        this.pasta_tmp = pasta_tmp;
    }
    
    public String getPastaTmp() {
        return pasta_tmp;
    }
    
    public void setPastaJasper(String pasta_jasper) {
        this.pasta_jasper = pasta_jasper;
    }
    
    public String getPastaJasper() {
        return pasta_jasper;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getSenha() {
        return senha;
    }
    
}