package br.com.acstoigo.controller;

import br.com.acstoigo.domain.Relatorio;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import br.com.acstoigo.util.Conexoes;
import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.commons.io.IOUtils;

/**
 * @author Chrystian Toigo
 */
public class RelatorioController {

    public static final String MSG_RESULTADO_SEM_PAGINAS = "Nenhuma informação encontrada com os parâmetros informados.";
    public static final String SUBREPORT_DIR = "SUBREPORT_DIR";

    public static String geraUrl(Relatorio vo) throws Throwable {
        valida(vo);
        try {
            geraJasper(vo);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (JRException e) {
            System.out.println(e.getMessage());
        }
        return "URL";
    }

    public static byte[] geraArquivo(Relatorio vo) throws Throwable {
        try {
            valida(vo);
            geraJasper(vo);
            return IOUtils.toByteArray(new FileInputStream(vo.getNomeRelatorioGerado()));
        } catch (IOException e) {
            throw new Exception(e.getMessage());
        }
    }

    private static void valida(Relatorio vo) {
        if (vo == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        if (vo.getNome() == null) {
            throw new RuntimeException("VO: atributo nome do relatorio não informado");
        }
        if (vo.getFormato() == null) {
            throw new RuntimeException("VO: atributo nome do relatorio não informado");
        }
    }

    private static void geraJasper(Relatorio vo) throws JRException, ClassNotFoundException, SQLException, IOException {
        String relatorioFile = vo.getPastaJasper() + File.separator + vo.getNome() + ".jasper";
        String nomeRelatorioExportado = vo.getNome() + "-" + System.currentTimeMillis() + "." + vo.getFormato().toLowerCase();

        Locale locale = new Locale("pt", "BR");
        vo.getParametros().put(JRParameter.REPORT_LOCALE, locale);
        vo.getParametros().put(SUBREPORT_DIR, vo.getPastaJasper() + File.separator);

        Conexoes conexao = new Conexoes();

        try {
            FileInputStream reportStream = new FileInputStream(relatorioFile);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(reportStream);

            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(bufferedInputStream);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, vo.getParametros(), conexao.getConexao(vo));

            if (jasperPrint.getPages().isEmpty()) {
                System.out.println(MSG_RESULTADO_SEM_PAGINAS + " - " + vo.getNome() + " com parâmetros " + vo.getParametros());
            } else {
                vo.setNomeRelatorioGerado(vo.getPastaTmp() + File.separator + nomeRelatorioExportado);
                JasperExportManager.exportReportToPdfFile(jasperPrint, vo.getNomeRelatorioGerado());
            }
        } catch (JRException e) {
            System.out.println(e.getMessage());
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }

        conexao.Close();
    }

}
