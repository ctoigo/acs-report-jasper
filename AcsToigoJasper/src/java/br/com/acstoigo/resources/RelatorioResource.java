package br.com.acstoigo.resources;

import br.com.acstoigo.domain.Relatorio;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.acstoigo.controller.RelatorioController;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * @author Chrystian Toigo
 */
@Path("/gerarRelatorio")
public class RelatorioResource {

    @POST
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getArquivo(String xml) {

        XStream xstream = new XStream(new DomDriver());
        xstream.alias("relatorio", Relatorio.class);
        Relatorio vo = (Relatorio) xstream.fromXML(xml);
        
        try {
            return Response.ok(RelatorioController.geraArquivo(vo)).header("Content-Disposition", "attachment; filename=" + vo.getNome() + "." + vo.getFormato()).build();
        } catch (Throwable e) {
            return Response.status(Status.PRECONDITION_FAILED).entity(e.getMessage()).type("text/plain").build();
        }

    }
}
