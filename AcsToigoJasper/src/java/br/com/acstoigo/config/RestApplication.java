package br.com.acstoigo.config;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import br.com.acstoigo.resources.RelatorioResource;

/**
 * @author Chrystian Toigo
 */
@ApplicationPath("/")
public class RestApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet<Class<?>>();
        classes.add(RelatorioResource.class);
        return classes;
    }
}