package br.com.acstoigo.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import br.com.acstoigo.domain.Relatorio;

/**
 * @author Chrystian Toigo
 */
public class Conexoes {

    private static Connection conexaoPostGres = null;

    public Connection getConexao(Relatorio vo) {
        String url_Conn = vo.getUrl().trim();
        String dbUser = vo.getUsuario().trim();
        String dbPassword = vo.getSenha().trim();

        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
            conexaoPostGres = DriverManager.getConnection(url_Conn, dbUser, dbPassword);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return conexaoPostGres;
    }

    public Conexoes() {
    }

    public void Close() {
        try {
            this.conexaoPostGres.close();
            this.conexaoPostGres = null;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
